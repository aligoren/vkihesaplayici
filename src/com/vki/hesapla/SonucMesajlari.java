/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vki.hesapla;

/**
 *
 * @author ali
 */
public class SonucMesajlari {
    
    String sonuc = "SONUÇ:\n\n";
    
    String[] tahmin = {"\n\nDurum: Zayıf", "\n\nDurum: Normal",
        "\n\nDurum: Kilolu", "\n\nDurum: 1. Derece Obez",
          "\n\nDurum: 2. Derece Obez", "\n\nDurum: Aşırı Obez"};
    
    String zayif = "\n\nOneri:\n\nBoyunuza göre uygun ağırlıkta olmadığınızı,"
                      + " zayıf olduğunuzu gösterir. Zayıflık, bazı hastalıklar için risk oluşturan "
                      + "ve istenmeyen bir durumdur. Boyunuza uygun"
                      + " ağırlığa erişmeniz için yeterli ve dengeli beslenmeli, "
                      + "beslenme alışkanlıklarınızı geliştirmeye özen göstermelisiniz."
                      + "\n"
                      + "\nEn kısa sürede doktorunuza başvurmayı unutmayın."
                      + "Daha sağlıklı bilgileri ondan almanız gerekir";
    
    String normal = "\n\nÖneri:\n\nBoyunuza göre uygun ağırlıkta olduğunuzu gösterir. \nYeterli ve"
            + " dengeli beslenerek ve düzenli fiziksel aktivite yaparak"
            + " bu ağırlığınızı korumaya özen gösteriniz.";
    
    String kilolu = "\n\nÖneri:\n\nBoyunuza göre vücut ağırlığınızın fazla olduğunu gösterir."
            + " \nFazla kilolu olma durumu gerekli önlemler alınmadığı "
            + "takdirde pek çok hastalık için"
            + " risk faktörü olan obeziteye (şişmanlık) yol açar.";
    
    String birinciSinifObez = "\n\nÖneri:\n\nBoyunuza göre vücut ağırlığınızın fazla olduğunu "
            + "bir başka deyişle şişman olduğunuzun "
            + "bir göstergesidir. Şişmanlık, kalp-damar hastalıkları, diyabet,"
            + " hipertansiyon v.b. kronik hastalıklar için risk faktörüdür. "
            + "Bir sağlık kuruluşuna başvurarak hekim / diyetisyen kontrolünde"
            + " zayıflayarak normal ağırlığa inmeniz sağlığınız açısından çok önemlidir."
            + " Lütfen, sağlık kuruluşuna başvurunuz.";
    
    String ikinciSinifObez = "\n\nÖneri:\n\nBoyunuza göre vücut ağırlığınızın fazla olduğunu bir başka deyişle şişman olduğunuzun"
            + " bir göstergesidir. Şişmanlık, kalp-damar hastalıkları,"
            + " diyabet, hipertansiyon v.b. kronik hastalıklar için risk faktörüdür."
            + " Bir sağlık kuruluşuna başvurarak hekim / diyetisyen kontrolünde zayıflayarak "
            + "normal ağırlığa inmeniz sağlığınız açısından çok önemlidir. "
            + "Lütfen, sağlık kuruluşuna başvurunuz.";
    
    String ucuncuSinifObez = "\n\nÖneri:\n\nBoyunuza göre vücut ağırlığınızın fazla olduğunu bir başka deyişle şişman"
            + " olduğunuzun bir göstergesidir. Şişmanlık, kalp-damar hastalıkları, diyabet,"
            + " hipertansiyon v.b. kronik hastalıklar için risk faktörüdür."
            + " Bir sağlık kuruluşuna başvurarak hekim / diyetisyen kontrolünde zayıflayarak normal"
            + " ağırlığa inmeniz sağlığınız açısından çok önemlidir. "
            + "Lütfen, sağlık kuruluşuna başvurunuz.";
}
